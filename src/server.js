import express from "express";
import { ApolloServer } from "apollo-server-express";
import typeDefs from './Tickets/typeDefs'
import resolvers from './Tickets/resolvers'
import authenticate from './services/authenticate'

const PORT = 4001;
/**
 * TODO: Your task is implementing the resolvers. Go through the README first.
 * TODO: Your resolvers below will need to implement the typedefs given above.
 */
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => {
    const tokenWithBearer = req.headers.authorization || ''
    const token = tokenWithBearer.split(' ')[1]
    const authorizationToken = authenticate.getUser(token);
    if(!authorizationToken)
      return new Error("You must be logged in");
      
    return { authorizationToken };
  }
});

const app = express();
server.applyMiddleware({ app });

app.listen({ port: PORT }, () => {
  console.log(`Server ready at: http://localhost:${PORT}${server.graphqlPath}`);
});
