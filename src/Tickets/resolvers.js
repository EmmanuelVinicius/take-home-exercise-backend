import jwt from 'jsonwebtoken';
import { models } from "./../db";

const resolvers = {
    Query: {
        /**
         * We have implemented this first query for you to set up an initial pattern.
         */
        tickets: async (root, args, context) => {
            const result = models.Ticket.findAll(
                {
                    where: {
                        parentId: null
                    }
                }
            );

            return result;
        },
        ticket: async (root, { id }, context) => {
            return models.Ticket.findByPk(id)

        }
    },
    Ticket: {
        children: ticket => {
            return models.Ticket.findAll({
                where: {
                    parentId: ticket.id
                }
            })
        }
    },
    Mutation: {
        login: async (root, data, context) => {
            const token = jwt.sign(
                { token: data.name },
                'secret',
                { expiresIn: '1d' },
            )
            return { token };
        },
        createTicket: async (root, data, context) => {
            if (!context.authorizationToken)
                return new Error("You must be logged in");

            return models.Ticket.create(data)

        },
        updateTicket: async (root, { id, title }, context) => {
            if (!context.authorizationToken)
                return new Error("You must be logged in");

            const updated = await models.Ticket.update({ title }, {
                where: { id }
            });

            if (updated)
                return models.Ticket.findByPk(id);

            throw Error("This ticket could not be updated.");

        },
        toggleTicket: async (root, { id, isCompleted }, context) => {
            if (!context.authorizationToken)
                return new Error("You must be logged in");

            const updated = await models.Ticket.update({ isCompleted }, {
                where: { id }
            });

            if (updated)
                return models.Ticket.findByPk(id);

            throw Error("This ticket could not be updated.");

        },
        removeTicket: async (root, { id }, context) => {
            if (!context.authorizationToken)
                return new Error("You must be logged in");

            return models.Ticket.destroy({
                where: { id }
            });

        },
        addChildrenToTicket: async (root, { parentId, childrenIds }, context) => {
            if (!context.authorizationToken)
                return new Error("You must be logged in");

            const updated = models.Ticket.update({ parentId }, {
                where: {
                    id: childrenIds
                }
            });

            if (updated)
                return models.Ticket.findByPk(parentId);
        },
        setParentOfTicket: async (root, { parentId, childId }, context) => {
            if (!context.authorizationToken)
                return new Error("You must be logged in");

            const updated = models.Ticket.update({ parentId }, {
                where: {
                    id: childId
                }
            });

            if (updated)
                return models.Ticket.findByPk(parentId);
        },
        removeParentFromTicket: async (root, { id }, context) => {
            if (!context.authorizationToken)
                return new Error("You must be logged in");

            const updated = await models.Ticket.update({ parentId: null }, {
                where: { id }
            });

            if (updated)
                return models.Ticket.findByPk(id);

            throw Error("This ticket could not be updated.");

        }
    }
};

module.exports = resolvers;