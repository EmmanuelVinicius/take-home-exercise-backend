import { gql } from "apollo-server-express";

const typeDefs = gql`
  type Ticket {
    id: ID!
    title: String!
    isCompleted: Boolean!
    children: [Ticket]!
  }

  type Token {
    token: String!
  }
  
  type Query {
    # return a list of all root level (parentless) tickets.
    tickets: [Ticket]!

    # return the ticket with the given id
    ticket(id: ID!): Ticket
  }

  type Mutation {
    # to login, you must copy the return of this method and paste at http headers section of Graphql Playground
    # Set the header like this:
    #  {
    #   "Authorization": "Bearer [your returned token]"
    #  }
    login(name: String!): Token!

    # create a ticket with the given params
    createTicket(title: String!, isCompleted: Boolean): Ticket

    # update the title of the ticket with the given id
    updateTicket(id: ID!, title: String!): Ticket

    # update ticket.isCompleted as given
    toggleTicket(id: ID!, isCompleted: Boolean!): Ticket

    # delete this ticket
    removeTicket(id: ID!): Boolean!

    # every children in childrenIds gets their parent set as parentId
    addChildrenToTicket(parentId: ID!, childrenIds: [ID!]!): Ticket

    # the ticket with id: childId gets the ticket with id: parentId as its new parent
    setParentOfTicket(parentId: ID!, childId: ID!): Ticket

    # the ticket with the given id becomes a root level ticket
    removeParentFromTicket(id: ID!): Ticket
  }
`;
module.exports = typeDefs;